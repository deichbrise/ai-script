\select@language {ngerman}
\contentsline {section}{\numberline {1}Einf\IeC {\"u}hrung}{3}{section.1}
\contentsline {section}{\numberline {2}Grundlagen}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Logik}{4}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Propositional Logic}{5}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}First-order predicate logic (PL1)}{5}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}Horn Logic}{5}{subsubsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.4}Sonstige Logik}{6}{subsubsection.2.1.4}
\contentsline {section}{\numberline {3}Prolog}{7}{section.3}
\contentsline {subsection}{\numberline {3.1}Ein einfaches Beispiel}{7}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Ein schwieriges Beispiel: Schubert's Steamroller}{10}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Einf\IeC {\"u}hrung}{12}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Syntax}{13}{subsection.3.4}
\contentsline {subsubsection}{\numberline {3.4.1}Atoms}{13}{subsubsection.3.4.1}
\contentsline {subsubsection}{\numberline {3.4.2}Numbers}{13}{subsubsection.3.4.2}
\contentsline {subsubsection}{\numberline {3.4.3}Constants}{14}{subsubsection.3.4.3}
\contentsline {subsubsection}{\numberline {3.4.4}Variables}{14}{subsubsection.3.4.4}
\contentsline {subsubsection}{\numberline {3.4.5}Complex Terms}{14}{subsubsection.3.4.5}
\contentsline {subsubsection}{\numberline {3.4.6}Arity}{14}{subsubsection.3.4.6}
\contentsline {subsection}{\numberline {3.5}Unification}{15}{subsection.3.5}
\contentsline {subsubsection}{\numberline {3.5.1}Defintion}{15}{subsubsection.3.5.1}
\contentsline {subsubsection}{\numberline {3.5.2}Beipiele}{16}{subsubsection.3.5.2}
\contentsline {subsubsection}{\numberline {3.5.3}Occurs Check}{17}{subsubsection.3.5.3}
\contentsline {subsubsection}{\numberline {3.5.4}Programming with Unification}{18}{subsubsection.3.5.4}
\contentsline {subsection}{\numberline {3.6}Lists}{19}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}Proof Strategies}{20}{subsection.3.7}
\contentsline {subsubsection}{\numberline {3.7.1}Proof Search}{20}{subsubsection.3.7.1}
\contentsline {subsubsection}{\numberline {3.7.2}Proof by Refutation}{21}{subsubsection.3.7.2}
\contentsline {subsubsection}{\numberline {3.7.3}Substitutions}{23}{subsubsection.3.7.3}
\contentsline {subsubsection}{\numberline {3.7.4}Prolog Prove Loop (SLD-Resolution)}{25}{subsubsection.3.7.4}
\contentsline {subsubsection}{\numberline {3.7.5}Proof by Induction}{26}{subsubsection.3.7.5}
\contentsline {subsection}{\numberline {3.8}Natural Language to Prolog Program Translation}{26}{subsection.3.8}
\contentsline {subsubsection}{\numberline {3.8.1}Normal Form}{27}{subsubsection.3.8.1}
\contentsline {subsubsection}{\numberline {3.8.2}Prenex Form}{27}{subsubsection.3.8.2}
\contentsline {subsubsection}{\numberline {3.8.3}Skolemization}{27}{subsubsection.3.8.3}
\contentsline {subsubsection}{\numberline {3.8.4}Conjunctive Normal Form}{28}{subsubsection.3.8.4}
\contentsline {subsection}{\numberline {3.9}Grammar}{28}{subsection.3.9}
\contentsline {subsubsection}{\numberline {3.9.1}Implementierung}{29}{subsubsection.3.9.1}
\contentsline {subsection}{\numberline {3.10}Patterns}{29}{subsection.3.10}
\contentsline {subsection}{\numberline {3.11}H\IeC {\"a}ufige Probleme in der Programmierung mit Prolog}{29}{subsection.3.11}
\contentsline {subsubsection}{\numberline {3.11.1}Syntax Error: Operation expected}{29}{subsubsection.3.11.1}
\contentsline {subsubsection}{\numberline {3.11.2}Undefined procedire: <Predicate name> / <arity>}{30}{subsubsection.3.11.2}
